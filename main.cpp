#include <QCoreApplication>
#include <QDebug>
#include "hook.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Hook hook;
    hook.start();
    return a.exec();
}
