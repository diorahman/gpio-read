#ifndef HOOK_H
#define HOOK_H

#include <QObject>

class QTimer;
class QFile;

class Hook : public QObject
{
    Q_OBJECT
public:
    explicit Hook(QObject *parent = 0);
    void start();

private:
    QTimer *m_timer;
    void read();
    int m_currentVal;
};

#endif // HOOK_H
