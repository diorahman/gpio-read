#-------------------------------------------------
#
# Project created by QtCreator 2015-09-22T11:14:59
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = yellow
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    hook.cpp

target.path = /home/diorahman/apps
INSTALLS += target

HEADERS += \
    hook.h

